package com.github.ruchirade;

import java.util.*;
import java.lang.*;
import org.apache.log4j.*;

class Calculator{
	final static Logger logger = Logger.getLogger(Calculator.class);

	Stack stack = new Stack(); //calculation stack
	HashMap map = new HashMap(); //to keep track of "let" variables
	int numLet=0; //to keep track of number of "let" remaining in stack
	Double result;

	public Double calculate(String expression){
		String[] arr = expression.split("\\s*(,|\\(|\\))\\s*");

		for(int index=0; index<arr.length; index++){

			String current = arr[index].trim();
			if(current.isEmpty())
				continue;
			try{
				double value = Double.parseDouble(current);
				stack.push(value);
			}catch(Exception e){
				if(current.length()==1){
					if(!arr[index-1].equals("let") && map.containsKey(current.charAt(0)))
						stack.push(map.get(current.charAt(0)));
					else
						stack.push(current.charAt(0));
				}
				else{
					stack.push(current);
					if(current.equals("let"))
						++numLet;
				}
			}
			logger.info(Arrays.toString(stack.toArray()));
			//Check if the stack can be reduced
			reduceStack();
			logger.info(Arrays.toString(stack.toArray()));
		}

		if(stack.size()>1){
			reduceStack(); 
		}

		result = (Double) stack.pop();

		return result;
	}

	private void reduceStack(){
		try{
			if(stack.peek() instanceof Double){ //if top element is a double
				Double var2 = (Double) stack.pop();
				if(stack.peek() instanceof Double){ //if second from top element is double
					Double var1 = (Double) stack.pop();
					if(stack.peek() instanceof String){ //reduce stack
						String operator = (String) stack.pop();
						if(operator.equals("add")){
							stack.push(var1+var2);
							if(stack.size()>1) reduceStack();
							return;
						}
						else if(operator.equals("sub")){
							stack.push(var1-var2);
							if(stack.size()>1) reduceStack();
							return;
						}
						else if(operator.equals("mult")){
							stack.push(var1*var2);
							if(stack.size()>1) reduceStack();
							return;
						}
						else if(operator.equals("div")){
							stack.push(var1/var2);
							if(stack.size()>1) reduceStack();
							return;
						}
					}
					return;
				}
				else if(stack.peek() instanceof Character){
					Character var1 = (Character) stack.pop();
					if(stack.peek() instanceof String){ //reduce stack
						String operator = (String) stack.pop();
						if(operator.equals("let")){
							map.put(var1, var2);
							return;
						}
					}
				}		
				stack.push(var2);
				return;
			}
			return;
		} catch (Exception e){
			logger.error(e);
			return;
		}
	}
}